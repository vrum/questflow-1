# README #

This README would normally document whatever steps are necessary to get your application up and running.

### QuestFlow 1 ###

QuestFlow 1 is a Quest and Dialogue system for Unity 3D 5.x line written in C#.

QuestFlow 1 is Apache 2.0 licensed

If licenses are TLDR for you: yes you can make a game with this and sell the game if you wish!

### QuestFlow 1 includes:###

* scriptable object's for Campaign, Quests, NPC's, Conversations, Locations, and Achievements
* Unity Editor Scripts for creating the scriptable objects
* Unity Editor Windows for editing the scriptable objects
* an Observer pattern based Quest Event Manager script to use in your implementation of the system in game.
* some generic NPC icons to get you started prototyping
* a sample scene demoing the QuestEventManager

### Quest Flow 1 does not include: ###

* A gameplay implementation.

### How do I get set up? ###

#### Option 1: Import the QuestFlow1.unitypackage ####

If you just want to use the system (including modifying the code to suit your own project needs if neccesary) 
then you can just download the QuestFlow1.unitypackage to your system and then inside of Unity 3D click on the
top menu bar Assets > Import Package > Custom Package and navigate to the location on your local disk where you 
saved the package and import it.

#### Option 2: Download the source code ####

If you have an interest in contributing to Quest Flow 1 then you would download/branch the sourcecode from bitbucket.org. 
You will get all of the same content as the unitypackage mentioned above, however if you want to make your changes under
source control and potentially contribute back to the project you will be able to do so.

### Dependencies###

Unity 3D 5.x is required to run this. The code is written in C#, but there's no reason you couldn't write your game play
implementation in another language if you are more comfortable with something else.

### Database configuration ###

QuestFlow 1 depends on scriptable objects used as in game databases.  You can create a new scriptable object/database 
by using either the Tools > QuestFlow menu or by launching one of the QuestFlow Editor Windows via Window menu.
Each QuestFlow Editor gives the option to create a new sciprtable object if none is found, or open an existing one with a file 
navigator.

### Implementing in gameplay ###

QuestFlow 1 does not include a game play implementation; this is the back end scriptable object/database for a quest system.

Because gameplay is very implementation specific that is left to you at this time. However if a contributor comes up with something
really slick I woudl consider merging it.


### Contribution guidelines ###

#### Content Direction####

This is a beta 1.0 product at this time; its unclear which direction it might take in the future. 
Large architectural changes might be better forked but I'm willing to discuss ideas.

Serialization/Exporting/Importing of quest data in to some form common format (XML,JSON,CSV, etc) would be
a welcome addition but wasn't a 1.0 consideration.

### Who do I talk to? ###

* Repo owner or admin

ckidwell at infernohawke (com) is the owner of this work if you need to reach out/contact.