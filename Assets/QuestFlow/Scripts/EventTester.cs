﻿using QuestFlow;
using System;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class EventTester : MonoBehaviour
{
    public InputField enterANumber;
    public Text messageReceivedText;

    // Use this for initialization
    private void Start()
    {
        QuestFlowEventManager.Instance.AddObserver(this);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void sendMessage()
    {
        if (enterANumber.text != null && enterANumber.text != "")
        {
            int test = Int32.Parse(enterANumber.text);
            QuestEvent questEvent = new QuestEvent();
            questEvent.eventID = test;
            QuestFlowEventManager.Instance.PostQuestEvent(this, questEvent);
        }
    }

    public void questevent(QuestEvent _event)
    {
        messageReceivedText.text = _event.eventID.ToString();
    }
}