﻿using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    [System.Serializable]
    public class QuestItem
    {
        public string questName;            // what is the name of this quest
        public string questText;            // the body of quest text
        public int questID;                 // what is the internal ID of this quest
        public Sprite npcIcon;              // if an NPC icon is to be used , well here it is
        public AudioClip questSound;        // a sound to play when this quest dialogue opens

        public bool isRootQuest;            // is this a top level or root quest
        public bool addToJournal;

        public int parentID;                //nullable,  if it is not a root conversation what is the parentID
        public List<int> childIDs;          // what children are under this conversation

        public int unlockingKey;            // nullable, is there an id or key to unlock this Item
        public List<int> unlocksKeys;       // are there any id(s) or key(s) this item unlocks?

        public bool completed;             // is this quest complete
        public bool shown;                 // if this item was previously show (you might grey it out - or not show again)
        public bool showOnCompletion;      // once complete should this show up any longer

        public int experienceReward;        // amount of XP to be rewarded
        public GameObject gameObjectReward; // a gameobject to offer as reward
        public int cashReward;              // a cash reward
        public bool proceduralReward;       // if this reward is a procedurally generated reward

        public int acheivementID;           // if an achievement is associated with this quest what is its ID
    }
}