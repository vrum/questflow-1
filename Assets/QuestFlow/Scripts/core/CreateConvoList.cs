﻿using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace QuestFlow
{
    public class CreateConvoList : MonoBehaviour
    {
#if UNITY_EDITOR

        [MenuItem("Tools/QuestFlow/Create Conversation List")]
#endif
        public static ConvoList Create()
        {
            ConvoList asset = ScriptableObject.CreateInstance<ConvoList>();

            AssetDatabase.CreateAsset(asset, "Assets/ConversationList.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            return asset;
        }

        public static ConvoList CreateByName(string _name)
        {
            ConvoList asset = ScriptableObject.CreateInstance<ConvoList>();

            string assetPath = "Assets/";
            string assetPathName = assetPath + _name + ".asset";

            AssetDatabase.CreateAsset(asset, assetPathName);
            AssetDatabase.SaveAssets();

            return asset;
        }
    }
}