﻿using QuestFlow;
using UnityEditor;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QuestFlowCampaignEditor : EditorWindow
{
    public Campaign campaign;

    [MenuItem("Window/QuestFlow Campaign Editor %#c")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(QuestFlowCampaignEditor));
    }

    private void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            campaign = AssetDatabase.LoadAssetAtPath(objectPath, typeof(Campaign)) as Campaign;
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("QuestFlow Campaign Editor", EditorStyles.boldLabel);
        if (campaign != null)
        {
            if (GUILayout.Button("Show Campaign"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = campaign;
            }
            if (GUILayout.Button("Open Campaign"))
            {
                OpenItemList();
            }
            if (GUILayout.Button("New Campaign"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = campaign;
            }
        }

        GUILayout.EndHorizontal();

        if (campaign == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New Campaign List", GUILayout.ExpandWidth(false)))
            {
                CreateNewCampaignList();
            }
            if (GUILayout.Button("Open Existing Campaign List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(20);

        if (campaign != null && campaign.campaign != null)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Space(10);

            GUILayout.Space(60);

            if (GUILayout.Button("Add Campaign", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
            if (GUILayout.Button("Delete Campaign", GUILayout.ExpandWidth(false)))
            {
                DeleteItem();
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();

            campaign.campaign.campaignID = EditorGUILayout.IntField("Campaign ID", campaign.campaign.campaignID);
            campaign.campaign.campaignName = EditorGUILayout.TextField("Campaign Name", campaign.campaign.campaignName as string);
            campaign.campaign.campaignDescription = EditorGUILayout.TextField("Campaign Description", campaign.campaign.campaignDescription as string);

            campaign.campaign.levelMin = EditorGUILayout.IntField("Min Level", campaign.campaign.levelMin);
            campaign.campaign.levelMax = EditorGUILayout.IntField("Max Level", campaign.campaign.levelMax);

            GUILayout.Space(10);
            campaign.campaign.splashGraphic = EditorGUILayout.ObjectField("Campaign backdrop or banner", campaign.campaign.splashGraphic, typeof(Sprite), false) as Sprite;

            GUILayout.Space(10);
            campaign.campaign.npcListName = EditorGUILayout.TextField("NPC List ", campaign.campaign.npcListName as string);
            campaign.campaign.questListName = EditorGUILayout.TextField("Quest List", campaign.campaign.questListName as string);
            campaign.campaign.conversationListName = EditorGUILayout.TextField("Conversation List", campaign.campaign.conversationListName as string);
            campaign.campaign.locationListName = EditorGUILayout.TextField("Location List", campaign.campaign.locationListName as string);
        }
        else if (campaign != null && campaign.campaign == null)
        {
            if (GUILayout.Button("Add your first Campaign", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(campaign);
        }
    }

    private void DeleteItem()
    {
        campaign.campaign = null;
    }

    private void AddItem()
    {
        CampaignItem newItem = new CampaignItem();
        newItem.campaignName = "Campaign Name";
        if (campaign.campaign == null)
        {
            campaign.campaign = new CampaignItem();
        }
    }

    private void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Campaign Database", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            campaign = AssetDatabase.LoadAssetAtPath(relPath, typeof(Campaign)) as Campaign;
            if (campaign)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }

    private void CreateNewCampaignList()
    {
        campaign = CreateCampaignDatabase.Create();
        if (campaign)
        {
            string relPath = AssetDatabase.GetAssetPath(campaign);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }
}