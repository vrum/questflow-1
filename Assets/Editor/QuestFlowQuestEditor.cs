﻿using QuestFlow;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QuestFlowQuestEditor : EditorWindow
{
    public QuestList questList;
    private int viewIndex = 1;

    [MenuItem("Window/QuestFlow Quest Editor %#e")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(QuestFlowQuestEditor));
    }

    private void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            questList = AssetDatabase.LoadAssetAtPath(objectPath, typeof(QuestList)) as QuestList;
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("QuestFlow Quest Editor", EditorStyles.boldLabel);
        if (questList != null)
        {
            if (GUILayout.Button("Show Quest List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = questList;
            }

            if (GUILayout.Button("Open Quest List"))
            {
                OpenItemList();
            }
            if (GUILayout.Button("New Quest List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = questList;
            }
        }
        GUILayout.EndHorizontal();

        if (questList == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New Quest List", GUILayout.ExpandWidth(false)))
            {
                CreateNewItemList();
            }
            if (GUILayout.Button("Open Existing Quest List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(20);

        if (questList != null && questList.questList != null)
        {
            if (questList.questList.Count > 0)
            {
                GUILayout.BeginHorizontal();

                GUILayout.Space(10);

                if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
                {
                    if (viewIndex > 1)
                    {
                        viewIndex--;
                    }
                }
                GUILayout.Space(10);
                if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
                {
                    if (viewIndex < questList.questList.Count)
                    {
                        viewIndex++;
                    }
                }

                GUILayout.Space(60);

                if (GUILayout.Button("Add Quest", GUILayout.ExpandWidth(false)))
                {
                    AddItem();
                }

                if (GUILayout.Button("Delete Quest", GUILayout.ExpandWidth(false)))
                {
                    DeleteItem(viewIndex - 1);
                }

                GUILayout.EndHorizontal();

                if (questList.questList.Count > 0)
                {
                    GUILayout.BeginHorizontal();

                    viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current Quest", viewIndex, GUILayout.ExpandWidth(false)), 1, questList.questList.Count);
                    EditorGUILayout.LabelField("of    " + questList.questList.Count.ToString() + "   items", "", GUILayout.ExpandWidth(false));
                    GUILayout.EndHorizontal();

                    questList.questList[viewIndex - 1].questName = EditorGUILayout.TextField("Quest Name", questList.questList[viewIndex - 1].questName as string);
                    questList.questList[viewIndex - 1].npcIcon = EditorGUILayout.ObjectField("Quest Icon", questList.questList[viewIndex - 1].npcIcon, typeof(Sprite), false) as Sprite;
                    questList.questList[viewIndex - 1].questSound = EditorGUILayout.ObjectField("Quest Audio Clip", questList.questList[viewIndex - 1].questSound, typeof(AudioClip), false) as AudioClip;
                    questList.questList[viewIndex - 1].addToJournal = (bool)EditorGUILayout.Toggle("Add to Quest Journal", questList.questList[viewIndex - 1].addToJournal, GUILayout.ExpandWidth(false));
                    questList.questList[viewIndex - 1].showOnCompletion = (bool)EditorGUILayout.Toggle("Show When completed", questList.questList[viewIndex - 1].showOnCompletion, GUILayout.ExpandWidth(false));

                    questList.questList[viewIndex - 1].isRootQuest = (bool)EditorGUILayout.Toggle("Is this a top level quest?", questList.questList[viewIndex - 1].isRootQuest, GUILayout.ExpandWidth(false));

                    if (!questList.questList[viewIndex - 1].isRootQuest)
                    {
                        questList.questList[viewIndex - 1].parentID = EditorGUILayout.IntField("Parent quest ID?", questList.questList[viewIndex - 1].parentID);
                    }

                    GUILayout.Space(10);
                    questList.questList[viewIndex - 1].experienceReward = EditorGUILayout.IntField("Experience Reward:", questList.questList[viewIndex - 1].experienceReward);
                    questList.questList[viewIndex - 1].cashReward = EditorGUILayout.IntField("Cash Reward:", questList.questList[viewIndex - 1].cashReward);
                    questList.questList[viewIndex - 1].proceduralReward = (bool)EditorGUILayout.Toggle("Cash Reward:", questList.questList[viewIndex - 1].proceduralReward, GUILayout.ExpandWidth(false));
                    questList.questList[viewIndex - 1].gameObjectReward = EditorGUILayout.ObjectField("GameObject Reward:", questList.questList[viewIndex - 1].gameObjectReward, typeof(GameObject), false) as GameObject;
                    questList.questList[viewIndex - 1].unlockingKey = EditorGUILayout.IntField("Key to unlock?", questList.questList[viewIndex - 1].unlockingKey);
                    questList.questList[viewIndex - 1].questText = EditorGUILayout.TextField("Base Item Description", questList.questList[viewIndex - 1].questText as string);
                    GUILayout.Space(10);
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(10);

                    if (GUILayout.Button("Add Child ID", GUILayout.ExpandWidth(false)))
                    {
                        AddChildItem();
                    }
                    if (questList.questList[viewIndex - 1].childIDs != null)
                    {
                        if (questList.questList[viewIndex - 1].childIDs.Count > 0)
                        {
                            if (GUILayout.Button("Delete Child ID", GUILayout.ExpandWidth(false)))
                            {
                                DeleteChildItem();
                            }
                        }

                        GUILayout.EndHorizontal();

                        if (questList.questList[viewIndex - 1].childIDs.Count > 0)
                        {
                            int _i = questList.questList[viewIndex - 1].childIDs.Count;

                            for (int x = 0; x < _i; x++)
                            {
                                questList.questList[viewIndex - 1].childIDs[x] = EditorGUILayout.IntField("Child :", questList.questList[viewIndex - 1].childIDs[x], GUILayout.ExpandWidth(false));
                            }
                        }
                    }

                    GUILayout.Space(10);
                }
                else
                {
                    GUILayout.Label("This quest database is empty");
                }
            }
        }
        else if (questList != null && questList.questList == null)
        {
            if (GUILayout.Button("Add your first Quest", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(questList);
        }
    }

    private void AddChildItem()
    {
        int newItem = 0;
        if (questList.questList[viewIndex - 1].childIDs == null)
        {
            questList.questList[viewIndex - 1].childIDs = new List<int>();
        }
        questList.questList[viewIndex - 1].childIDs.Add(newItem);
    }

    private void DeleteChildItem()
    {
        questList.questList[viewIndex - 1].childIDs.RemoveAt(questList.questList[viewIndex - 1].childIDs.Count - 1);
    }

    private void DeleteItem(int index)
    {
        questList.questList.RemoveAt(index);
    }

    private void AddItem()
    {
        QuestItem newItem = new QuestItem();
        newItem.questName = "New Quest";
        if (questList.questList == null)
        {
            questList.questList = new List<QuestItem>();
        }
        questList.questList.Add(newItem);
        viewIndex = questList.questList.Count;
    }

    private void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Quest Database", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            questList = AssetDatabase.LoadAssetAtPath(relPath, typeof(QuestList)) as QuestList;
            if (questList)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }

    private void CreateNewItemList()
    {
        viewIndex = 1;
        questList = CreateQuestDatabase.Create();
        if (questList)
        {
            string relPath = AssetDatabase.GetAssetPath(questList);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }
}