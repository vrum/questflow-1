﻿using QuestFlow;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QuestFlowLocationEditor : EditorWindow
{
    public LocationItemList locList;
    private int viewIndex = 1;

    [MenuItem("Window/QuestFlow Location Editor %#l")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(QuestFlowLocationEditor));
    }

    private void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            locList = AssetDatabase.LoadAssetAtPath(objectPath, typeof(LocationItemList)) as LocationItemList;
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("QuestFlow Location Editor", EditorStyles.boldLabel);
        if (locList != null)
        {
            if (GUILayout.Button("Show Location List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = locList;
            }

            if (GUILayout.Button("Open Location List"))
            {
                OpenItemList();
            }
            if (GUILayout.Button("New Location List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = locList;
            }
        }

        GUILayout.EndHorizontal();

        if (locList == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New Location List", GUILayout.ExpandWidth(false)))
            {
                CreateNewNPCList();
            }
            if (GUILayout.Button("Open Existing Location List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(20);

        if (locList != null && locList.locationList != null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);

            if (locList.locationList.Count > 0)
            {
                if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
                {
                    if (viewIndex > 1)
                    {
                        viewIndex--;
                    }
                }
                GUILayout.Space(10);
                if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
                {
                    if (viewIndex < locList.locationList.Count)
                    {
                        viewIndex++;
                    }
                }
            }

            GUILayout.Space(60);

            if (GUILayout.Button("Add Location", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }

            if (GUILayout.Button("Delete Location", GUILayout.ExpandWidth(false)) && locList.locationList.Count > 0)
            {
                DeleteItem(viewIndex - 1);
            }

            GUILayout.EndHorizontal();

            if (locList.locationList != null && locList.locationList.Count > 0)
            {
                GUILayout.BeginHorizontal();

                viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current Location", viewIndex, GUILayout.ExpandWidth(false)), 1, locList.locationList.Count);
                EditorGUILayout.LabelField("of    " + locList.locationList.Count.ToString() + "   items", "", GUILayout.ExpandWidth(false));
                GUILayout.EndHorizontal();

                locList.locationList[viewIndex - 1].locationName = EditorGUILayout.TextField("Location Name", locList.locationList[viewIndex - 1].locationName as string);
                locList.locationList[viewIndex - 1].description = EditorGUILayout.TextField("Location Description", locList.locationList[viewIndex - 1].description as string);

                GUILayout.Space(10);
                locList.locationList[viewIndex - 1].backdrop = EditorGUILayout.ObjectField("Location Backdrop", locList.locationList[viewIndex - 1].backdrop, typeof(Sprite), false) as Sprite;

                GUILayout.Space(10);
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Add NPC ID", GUILayout.ExpandWidth(false)))
                {
                    AddNPCChildItem();
                }

                if (locList.locationList[viewIndex - 1].npcs != null && locList.locationList[viewIndex - 1].npcs.Count > 0)
                {
                    if (GUILayout.Button("Delete NPC ID", GUILayout.ExpandWidth(false)))
                    {
                        DeleteNPCChildItem();
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.Space(10);

                if (locList.locationList[viewIndex - 1].npcs != null && locList.locationList[viewIndex - 1].npcs.Count > 0)
                {
                    int _index = locList.locationList[viewIndex - 1].npcs.Count;

                    for (int y = 0; y < _index; y++)
                    {
                        locList.locationList[viewIndex - 1].npcs[y] = EditorGUILayout.IntField("NPC ID :", locList.locationList[viewIndex - 1].npcs[y], GUILayout.ExpandWidth(false));
                    }
                }

                GUILayout.Space(20);
            }
            else
            {
                GUILayout.Label("This Location database is empty");
            }
        }
        else if (locList != null && locList.locationList == null)
        {
            if (GUILayout.Button("Add your first Location", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(locList);
        }
    }

    private void AddNPCChildItem()
    {
        int newItem = 0;
        if (locList.locationList[viewIndex - 1].npcs == null)
        {
            locList.locationList[viewIndex - 1].npcs = new List<int>();
        }
        locList.locationList[viewIndex - 1].npcs.Add(newItem);
    }

    private void DeleteNPCChildItem()
    {
        locList.locationList[viewIndex - 1].npcs.RemoveAt(locList.locationList[viewIndex - 1].npcs.Count - 1);
    }

    private void DeleteItem(int index)
    {
        locList.locationList.RemoveAt(index);
    }

    private void AddItem()
    {
        LocationItem newItem = new LocationItem();
        newItem.locationName = "Location Name";
        if (locList.locationList == null)
        {
            locList.locationList = new List<LocationItem>();
        }
        locList.locationList.Add(newItem);
        viewIndex = locList.locationList.Count;
    }

    private void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Location Database", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            locList = AssetDatabase.LoadAssetAtPath(relPath, typeof(LocationItemList)) as LocationItemList;
            if (locList)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }

    private void CreateNewNPCList()
    {
        viewIndex = 1;
        locList = CreateLocationItemList.Create();
        if (locList)
        {
            string relPath = AssetDatabase.GetAssetPath(locList);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }
}