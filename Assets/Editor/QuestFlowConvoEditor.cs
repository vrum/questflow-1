﻿using QuestFlow;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QuestFlowConvoEditor : EditorWindow
{
    public ConvoList conversationList;
    private int viewIndex = 1;

    [MenuItem("Window/QuestFlow Conversation Editor %#q")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(QuestFlowConvoEditor));
    }

    private void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            conversationList = AssetDatabase.LoadAssetAtPath(objectPath, typeof(ConvoList)) as ConvoList;
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("QuestFlow Conversation Editor", EditorStyles.boldLabel);
        if (conversationList != null)
        {
            if (GUILayout.Button("Show Conversation List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = conversationList;
            }
            if (GUILayout.Button("Open Conversation List"))
            {
                OpenItemList();
            }
            if (GUILayout.Button("New Conversation List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = conversationList;
            }
        }

        GUILayout.EndHorizontal();

        if (conversationList == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New Conversation List", GUILayout.ExpandWidth(false)))
            {
                CreateNewItemList();
            }
            if (GUILayout.Button("Open Existing Conversation List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(20);

        if (conversationList != null && conversationList.convoList != null)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Space(10);
            if (conversationList.convoList.Count > 0)
            {
                if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
                {
                    if (viewIndex > 1)
                    {
                        viewIndex--;
                    }
                }
                GUILayout.Space(10);
                if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
                {
                    if (viewIndex < conversationList.convoList.Count)
                    {
                        viewIndex++;
                    }
                }
            }

            GUILayout.Space(60);

            if (GUILayout.Button("Add Conversation", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
            if (GUILayout.Button("Delete Conversation", GUILayout.ExpandWidth(false)))
            {
                DeleteItem(viewIndex - 1);
            }

            GUILayout.EndHorizontal();

            if (conversationList.convoList.Count > 0)
            {
                GUILayout.BeginHorizontal();

                viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current Conversation", viewIndex, GUILayout.ExpandWidth(false)), 1, conversationList.convoList.Count);
                EditorGUILayout.LabelField("of    " + conversationList.convoList.Count.ToString() + "   items", "", GUILayout.ExpandWidth(false));
                GUILayout.EndHorizontal();

                conversationList.convoList[viewIndex - 1].conversationID = EditorGUILayout.IntField("Conversation ID", conversationList.convoList[viewIndex - 1].conversationID, GUILayout.ExpandWidth(false));

                conversationList.convoList[viewIndex - 1].isRootConversation = (bool)EditorGUILayout.Toggle("Is this a top level conversation?", conversationList.convoList[viewIndex - 1].isRootConversation, GUILayout.ExpandWidth(false));
                if (!conversationList.convoList[viewIndex - 1].isRootConversation)
                {
                    conversationList.convoList[viewIndex - 1].parentID = EditorGUILayout.IntField("Parent ID?", conversationList.convoList[viewIndex - 1].parentID);
                }

                conversationList.convoList[viewIndex - 1].questEntryText = EditorGUILayout.TextField("Entry Text", conversationList.convoList[viewIndex - 1].questEntryText as string);
                conversationList.convoList[viewIndex - 1].conversationText = EditorGUILayout.TextField("Text Body", conversationList.convoList[viewIndex - 1].conversationText as string);

                conversationList.convoList[viewIndex - 1].unlockingKey = EditorGUILayout.IntField("Key to unlock?", conversationList.convoList[viewIndex - 1].unlockingKey);
                conversationList.convoList[viewIndex - 1].unlocksKey = EditorGUILayout.IntField("ID this unlocks", conversationList.convoList[viewIndex - 1].unlocksKey);
                conversationList.convoList[viewIndex - 1].locationID = EditorGUILayout.IntField("Location ID", conversationList.convoList[viewIndex - 1].locationID);

                GUILayout.Space(10);
                conversationList.convoList[viewIndex - 1].soundToPlay = EditorGUILayout.ObjectField("Audio Clip", conversationList.convoList[viewIndex - 1].soundToPlay, typeof(AudioClip), false) as AudioClip;

                GUILayout.Space(10);

                EditorGUILayout.TextArea("If there is a specific NPC or NPC's involved in this conversation item add them below, otherwise the parent/quest giver NPC is used by default.");

                GUILayout.Space(10);
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Add NPC ID", GUILayout.ExpandWidth(false)))
                {
                    AddNPCChildItem();
                }

                if (conversationList.convoList[viewIndex - 1].npcs != null && conversationList.convoList[viewIndex - 1].npcs.Count > 0)
                {
                    if (GUILayout.Button("Delete NPC ID", GUILayout.ExpandWidth(false)))
                    {
                        DeleteNPCChildItem();
                    }
                }
                GUILayout.EndHorizontal();
                GUILayout.Space(10);
                if (conversationList.convoList[viewIndex - 1].npcs != null && conversationList.convoList[viewIndex - 1].npcs.Count > 0)
                {
                    int _index = conversationList.convoList[viewIndex - 1].npcs.Count;

                    for (int y = 0; y < _index; y++)
                    {
                        conversationList.convoList[viewIndex - 1].npcs[y] = EditorGUILayout.IntField("NPC ID :", conversationList.convoList[viewIndex - 1].npcs[y], GUILayout.ExpandWidth(false));
                    }
                }

                GUILayout.Space(20);
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Add Child ID", GUILayout.ExpandWidth(false)))
                {
                    AddChildItem();
                }

                if (conversationList.convoList[viewIndex - 1].childIDs != null && conversationList.convoList[viewIndex - 1].childIDs.Count > 0)
                {
                    if (GUILayout.Button("Delete Child ID", GUILayout.ExpandWidth(false)))
                    {
                        DeleteChildItem();
                    }
                }

                GUILayout.EndHorizontal();

                if (conversationList.convoList[viewIndex - 1].childIDs != null && conversationList.convoList[viewIndex - 1].childIDs.Count > 0)
                {
                    int _i = conversationList.convoList[viewIndex - 1].childIDs.Count;

                    for (int x = 0; x < _i; x++)
                    {
                        conversationList.convoList[viewIndex - 1].childIDs[x] = EditorGUILayout.IntField("Child :", conversationList.convoList[viewIndex - 1].childIDs[x], GUILayout.ExpandWidth(false));
                    }
                }
                GUILayout.Space(10);
            }
            else
            {
                GUILayout.Label("This Conversation database is empty");
            }
        }
        else if (conversationList != null && conversationList.convoList == null)
        {
            if (GUILayout.Button("Add your first Conversation", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(conversationList);
        }
    }

    private void AddNPCChildItem()
    {
        int newItem = 0;
        if (conversationList.convoList[viewIndex - 1].npcs == null)
        {
            conversationList.convoList[viewIndex - 1].npcs = new List<int>();
        }
        conversationList.convoList[viewIndex - 1].npcs.Add(newItem);
    }

    private void DeleteNPCChildItem()
    {
        conversationList.convoList[viewIndex - 1].npcs.RemoveAt(conversationList.convoList[viewIndex - 1].npcs.Count - 1);
    }

    private void AddChildItem()
    {
        int newItem = 0;
        if (conversationList.convoList[viewIndex - 1].childIDs == null)
        {
            conversationList.convoList[viewIndex - 1].childIDs = new List<int>();
        }
        conversationList.convoList[viewIndex - 1].childIDs.Add(newItem);
    }

    private void DeleteChildItem()
    {
        conversationList.convoList[viewIndex - 1].childIDs.RemoveAt(conversationList.convoList[viewIndex - 1].childIDs.Count - 1);
    }

    private void DeleteItem(int index)
    {
        conversationList.convoList.RemoveAt(index);
    }

    private void AddItem()
    {
        ConversationItem newItem = new ConversationItem();
        newItem.questEntryText = "New Conversation";
        if (conversationList.convoList == null)
        {
            conversationList.convoList = new List<ConversationItem>();
        }
        conversationList.convoList.Add(newItem);
        viewIndex = conversationList.convoList.Count;
    }

    private void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Quest Database", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            conversationList = AssetDatabase.LoadAssetAtPath(relPath, typeof(ConvoList)) as ConvoList;
            if (conversationList)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }

    private void CreateNewItemList()
    {
        viewIndex = 1;
        conversationList = CreateConvoList.Create();
        if (conversationList)
        {
            string relPath = AssetDatabase.GetAssetPath(conversationList);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }
}