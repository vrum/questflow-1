﻿using QuestFlow;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QuestFlowNPCEditor : EditorWindow
{
    public NPCList npcList;
    private int viewIndex = 1;

    [MenuItem("Window/QuestFlow NPC Editor %#n")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(QuestFlowNPCEditor));
    }

    private void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            npcList = AssetDatabase.LoadAssetAtPath(objectPath, typeof(NPCList)) as NPCList;
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("QuestFlow NPC Editor", EditorStyles.boldLabel);
        if (npcList != null)
        {
            if (GUILayout.Button("Show NPC List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = npcList;
            }
            if (GUILayout.Button("Open NPC List"))
            {
                OpenItemList();
            }
            if (GUILayout.Button("New NPC List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = npcList;
            }
        }

        GUILayout.EndHorizontal();

        if (npcList == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New NPC List", GUILayout.ExpandWidth(false)))
            {
                CreateNewNPCList();
            }
            if (GUILayout.Button("Open Existing NPC List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(20);

        if (npcList != null && npcList.npcItemList != null)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Space(10);

            if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex > 1)
                {
                    viewIndex--;
                }
            }
            GUILayout.Space(10);
            if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex < npcList.npcItemList.Count)
                {
                    viewIndex++;
                }
            }

            GUILayout.Space(60);

            if (GUILayout.Button("Add NPC", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
            if (GUILayout.Button("Delete NPC", GUILayout.ExpandWidth(false)))
            {
                DeleteItem(viewIndex - 1);
            }

            GUILayout.EndHorizontal();

            if (npcList.npcItemList.Count > 0)
            {
                GUILayout.BeginHorizontal();

                viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current NPC", viewIndex, GUILayout.ExpandWidth(false)), 1, npcList.npcItemList.Count);
                EditorGUILayout.LabelField("of    " + npcList.npcItemList.Count.ToString() + "   items", "", GUILayout.ExpandWidth(false));
                GUILayout.EndHorizontal();

                npcList.npcItemList[viewIndex - 1].npcID = EditorGUILayout.IntField("NPC ID", npcList.npcItemList[viewIndex - 1].npcID);
                npcList.npcItemList[viewIndex - 1].npcName = EditorGUILayout.TextField("NPC Name", npcList.npcItemList[viewIndex - 1].npcName as string);
                npcList.npcItemList[viewIndex - 1].description = EditorGUILayout.TextField("NPC Description", npcList.npcItemList[viewIndex - 1].description as string);

                GUILayout.Space(10);
                npcList.npcItemList[viewIndex - 1].npcIcon = EditorGUILayout.ObjectField("NPC Icon", npcList.npcItemList[viewIndex - 1].npcIcon, typeof(Sprite), false) as Sprite;

                GUILayout.Space(10);
            }
            else
            {
                GUILayout.Label("This NPC database is empty");
            }
        }
        else if (npcList != null && npcList.npcItemList == null)
        {
            if (GUILayout.Button("Add your first NPC", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(npcList);
        }
    }

    private void DeleteItem(int index)
    {
        npcList.npcItemList.RemoveAt(index);
    }

    private void AddItem()
    {
        NPCItem newItem = new NPCItem();
        newItem.npcName = "NPC Name";
        if (npcList.npcItemList == null)
        {
            npcList.npcItemList = new List<NPCItem>();
        }
        npcList.npcItemList.Add(newItem);
        viewIndex = npcList.npcItemList.Count;
    }

    private void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select NPC Database", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            npcList = AssetDatabase.LoadAssetAtPath(relPath, typeof(NPCList)) as NPCList;
            if (npcList)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }

    private void CreateNewNPCList()
    {
        viewIndex = 1;
        npcList = CreateNPCList.Create();
        if (npcList)
        {
            string relPath = AssetDatabase.GetAssetPath(npcList);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }
}