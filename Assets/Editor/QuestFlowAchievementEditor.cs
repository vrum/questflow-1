﻿using QuestFlow;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QuestFlowAchievementEditor : EditorWindow
{
    public AchievementList achievementList;
    private int viewIndex = 1;

    [MenuItem("Window/QuestFlow Achievement Editor %#a")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(QuestFlowAchievementEditor));
    }

    private void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            achievementList = AssetDatabase.LoadAssetAtPath(objectPath, typeof(AchievementList)) as AchievementList;
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("QuestFlow Achievement Editor", EditorStyles.boldLabel);
        if (achievementList != null)
        {
            if (GUILayout.Button("Show Achievement List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = achievementList;
            }
            if (GUILayout.Button("Open Achievement List"))
            {
                OpenItemList();
            }
            if (GUILayout.Button("New Achievement List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = achievementList;
            }
        }

        GUILayout.EndHorizontal();

        if (achievementList == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New Achievement List", GUILayout.ExpandWidth(false)))
            {
                CreateNewAchievementList();
            }
            if (GUILayout.Button("Open Existing Achievement List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(20);

        if (achievementList != null && achievementList.achievements != null)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Space(10);

            if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex > 1)
                {
                    viewIndex--;
                }
            }
            GUILayout.Space(10);
            if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex < achievementList.achievements.Count)
                {
                    viewIndex++;
                }
            }

            GUILayout.Space(60);

            if (GUILayout.Button("Add Achievement", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
            if (GUILayout.Button("Delete Achievement", GUILayout.ExpandWidth(false)))
            {
                DeleteItem(viewIndex - 1);
            }

            GUILayout.EndHorizontal();

            if (achievementList.achievements.Count > 0)
            {
                GUILayout.BeginHorizontal();

                viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current Achievement", viewIndex, GUILayout.ExpandWidth(false)), 1, achievementList.achievements.Count);
                EditorGUILayout.LabelField("of    " + achievementList.achievements.Count.ToString() + "   items", "", GUILayout.ExpandWidth(false));
                GUILayout.EndHorizontal();

                achievementList.achievements[viewIndex - 1].achievementID = EditorGUILayout.IntField("Achievement ID", achievementList.achievements[viewIndex - 1].achievementID);
                achievementList.achievements[viewIndex - 1].achievementName = EditorGUILayout.TextField("Achievement Name", achievementList.achievements[viewIndex - 1].achievementName as string);
                achievementList.achievements[viewIndex - 1].achievementDescription = EditorGUILayout.TextField("Achievement Description", achievementList.achievements[viewIndex - 1].achievementDescription as string);

                GUILayout.Space(10);
                achievementList.achievements[viewIndex - 1].achievementIcon = EditorGUILayout.ObjectField("Achievement Icon", achievementList.achievements[viewIndex - 1].achievementIcon, typeof(Sprite), false) as Sprite;

                GUILayout.Space(10);
            }
            else
            {
                GUILayout.Label("This Achievement database is empty");
            }
        }
        else if (achievementList != null && achievementList.achievements == null)
        {
            if (GUILayout.Button("Add your first Achievement", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(achievementList);
        }
    }

    private void DeleteItem(int index)
    {
        achievementList.achievements.RemoveAt(index);
    }

    private void AddItem()
    {
        AchievementItem newItem = new AchievementItem();
        newItem.achievementName = "Achievement Name";
        if (achievementList.achievements == null)
        {
            achievementList.achievements = new List<AchievementItem>();
        }
        achievementList.achievements.Add(newItem);
        viewIndex = achievementList.achievements.Count;
    }

    private void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Achievement Database", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            achievementList = AssetDatabase.LoadAssetAtPath(relPath, typeof(AchievementList)) as AchievementList;
            if (achievementList)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }

    private void CreateNewAchievementList()
    {
        viewIndex = 1;
        achievementList = CreateAchievementList.Create();
        if (achievementList)
        {
            string relPath = AssetDatabase.GetAssetPath(achievementList);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }
}